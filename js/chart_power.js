window.onload = function () {
  CanvasJS.addColorSet("dbWiki", [ "orange"]);

  var chart = new CanvasJS.Chart("chartContainer",
  {
    colorSet: "dbWiki",
    theme: "theme",
    animationEnabled: true,
    backgroundColor: "transparent",
    axisX: {
      labelFontColor: "white"
    },
    axisY:{
      labelFontColor: "white",
      includeZero: false
    },
    data: [
    {
      type: "line",
      lineThickness: 3,
      dataPoints: [
      { label: "Saga Dragon Ball", y: 910 },
      { label: "Saga Saiyajin", y: 32000 },
      { label: "Saga Frieza", y: 3000000 },
      { label: "Saga Androids", y: 7000000 },
      { label: "Saga Cell", y: 25500000 },
      { label: "Saga Boo", y: 80000000 },
      { label: "Saga Super", y: 180000000 }
      ]
    }


    ]
  });

chart.render();
}
